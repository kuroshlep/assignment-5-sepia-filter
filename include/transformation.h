#pragma once

#include "image.h"

struct image sepia( struct image const in);

struct image sepia_asm( struct image const in);

